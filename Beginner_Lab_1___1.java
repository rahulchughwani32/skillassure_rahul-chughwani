public class Main
{
	public static void main(String[] args) {
	    
	    //step 1-Declaration
	    double principle, rate_of_interest, time, simple_interest;
	    
	    //step 2-Set the values
	     principle=100;
	     rate_of_interest=5.00;
	     time=2.00;
	     
	     //step 3-set the formula
	      simple_interest = (principle * rate_of_interest * time) / 100;
	      
	     //step 4-Display
		  System.out.println("The Simple Interest Is = " + simple_interest);
	}
}
