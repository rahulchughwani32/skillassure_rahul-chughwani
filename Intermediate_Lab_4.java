public class Main
{
  public static void main (String[]args)
  {
    //step 1- Declaration
    int i, j, sum = 0;
    
    //step 2- using for loop 
    for(i=0;i<100;i++)
    {
        for(j=2;j<i;j++)
        {
            //step  3- condition
	        if (i % j == 0)
	        {
	             break;
	        }
	    }
     //step 4- again condition
    	if (i == j)
    	{
    	     System.out.print(i + ",");
    	}
    }
  }
}