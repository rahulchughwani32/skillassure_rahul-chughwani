public class Main
{
  public static void main (String[]args)
  {
    //step 1- Declaration
    int i = 1, j = 100, sum = 0;

    //step 2- for loop 
    for (i = 1; i < j; i++)

      //step 3- condition
      if (i % 2 == 1)
	sum = sum + i;

    //step 4- display
      System.out.print ("the sum of odd numbers is " + sum);

  }
}
