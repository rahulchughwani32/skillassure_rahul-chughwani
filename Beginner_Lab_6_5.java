public class Main
{
  public static void main (String[]args)
  {

    //step 1-Declaration
    int n1=1,n2=-2,i=1,j=10;
    System.out.print(n1 + "," + n2 + ",");
    
    //using for loop and display the output
    for (i = 1; i <= j; i++)
      {
          n1=n1+3;
          n2=n2-4;
	      System.out.print (n1 + "," + n2 + ",");
      }
  }
}
