public class Main
{
	public static void main(String[] args) {
	    
	    //step 1-Declaration
	     int i=0,j=0,k=0;
	    
	    //step 2-Set the values
	     i=25;
	     j=50;
	     
	     //step 3-set the formula for swapping the two numbers
	      k=i;
	      i=j;
	      j=k;
	      
	      //step 4-display
		  System.out.println(" the value of i is " + i + " \n the value of j is " + j);
		  
	}
}
